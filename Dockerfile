FROM golang:1.6.0-alpine

RUN mkdir -p /src/github.com/dynport/tobstarr/x/ts-fs
COPY main.go main_test.go /src/github.com/dynport/tobstarr/x/ts-fs/

ENV GOPATH=/

RUN go build -o /usr/local/bin/ts-fs github.com/dynport/tobstarr/x/ts-fs 


ENV PORT 80

ENTRYPOINT ["/usr/local/bin/ts-fs"]
