package main

import (
	"bytes"
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func main() {
	l := log.New(os.Stderr, "", 0)
	if err := run(l); err != nil {
		l.Fatal(err)
	}
}

type Logger interface {
	Printf(string, ...interface{})
}

func run(l Logger) error {
	dir := flag.String("d", ".", "Dir")
	flag.Parse()

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	if !strings.Contains(port, ":") {
		port = "0.0.0.0:" + port
	}
	log.Printf("running on port %q", port)
	if dir == nil {
		wd, err := os.Getwd()
		if err != nil {
			return err
		}
		dir = &wd
	}
	return http.ListenAndServe(port, fsHandler(*dir))
}

func isLinkedDir(path string) bool {
	ok, _ := isLinkedDirWithErr(path)
	return ok
}

func isLinkedDirWithErr(path string) (bool, error) {
	stat, err := os.Stat(path)
	if err != nil {
		return false, err
	} else if isSymlink(stat) {
		link, err := os.Readlink(path)
		if err != nil {
			return false, err
		}
		ls, err := os.Stat(link)
		if err != nil {
			return false, err
		}
		log.Printf("ls=%s dir=%t", ls.Name(), ls.IsDir())
		return ls.IsDir(), nil
	}
	return stat.IsDir(), nil
}

func isSymlink(info os.FileInfo) bool {
	return info.Mode()&os.ModeSymlink == os.ModeSymlink
}

var zone = time.Local

type entry struct {
	Name    string
	Dir     bool
	Size    *int64
	ModTime time.Time
}

var l = log.New(os.Stderr, "", 0)

func fsHandler(dir string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l.Printf("method=%s url=%s", r.Method, r.URL)
		w.Header().Set("Server", "nginx/1.6.2 (Ubuntu)")
		root := filepath.Join(dir, r.URL.Path)
		err := func() error {
			f, err := os.Open(root)
			if err != nil {
				if os.IsNotExist(err) {
					http.NotFound(w, r)
					return nil
				}
				return err
			}
			defer f.Close()
			if stat, err := f.Stat(); err != nil {
				return err
			} else if stat.IsDir() || isLinkedDir(root) {
				return renderDir(root, dir, w)
			} else {
				http.ServeContent(w, r, filepath.Base(root), stat.ModTime(), f)
			}
			return nil
		}()
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		return
	}
}

func loadEntries(dir string) (list []*entry, err error) {
	d, err := os.Stat(dir)
	if err != nil {
		return nil, err
	}
	err = filepath.Walk(strings.TrimSuffix(dir, "/")+"/", func(p string, info os.FileInfo, err error) error {
		if os.SameFile(d, info) {
			return nil
		}
		mod := info.ModTime()
		if info.IsDir() || isLinkedDir(p) {
			list = append(list, &entry{Name: filepath.Base(p) + "/", ModTime: mod, Dir: true})
			if info.IsDir() {
				return filepath.SkipDir
			}
		} else {
			siz := info.Size()
			list = append(list, &entry{Name: filepath.Base(p), ModTime: mod, Size: &siz})
		}
		return nil
	})
	if err != nil && err != filepath.SkipDir {
		return list, err
	}
	return list, nil
}

func renderDir(root, dir string, w io.Writer) error {
	entries, err := loadEntries(root)
	if err != nil {
		return err
	}
	return renderEntries(root, dir, entries, w)
}

const newline = "\r\n"

func renderEntries(root, dir string, list []*entry, o io.Writer) error {
	h, err := render(header, struct{ Dir string }{strings.TrimSuffix(strings.TrimPrefix(root, dir), "/") + "/"})
	if err != nil {
		return err
	}
	if _, err := io.WriteString(o, h+`<a href="../">../</a>`+newline); err != nil {
		return err
	}
	for _, e := range list {
		if _, err := io.WriteString(o, renderEntry(e)); err != nil {
			return err
		}
	}
	_, err = io.WriteString(o, `</pre><hr></body></html>`)
	return err
}

func renderEntry(e *entry) string {
	return makeLink(e.Name) + e.ModTime.In(zone).Format("02-Jan-2006 15:04") + formatSize(e.Size) + newline
}

func formatSize(in *int64) string {
	if in == nil {
		return strings.Repeat(" ", 19) + "-"
	}
	s := strconv.FormatInt(*in, 10)
	return strings.Repeat(" ", 20-len(s)) + s
}

const fileLength = 50

func makeLink(name string) string {
	displayName := name
	padding := fileLength + 1 - len(name)
	if len(name) > fileLength {
		displayName = name[0:fileLength-3] + "..&gt;"
		padding = 1
	}
	return `<a href="` + escapeURL(name) + `">` + displayName + "</a>" + strings.Repeat(" ", padding)
}

func escapeURL(in string) string {
	return strings.Replace(in, " ", "%20", -1)
}

const header = `<html>` + "\r" + `
<head><title>Index of {{ .Dir }}</title></head>` + "\r" + `
<body bgcolor="white">` + "\r" + `
<h1>Index of {{ .Dir }}</h1><hr><pre>`

func mustRender(t string, i interface{}) string {
	s, err := render(t, i)
	if err != nil {
		log.Fatal(err)
	}
	return s
}

func render(t string, i interface{}) (string, error) {
	tpl, err := template.New(t).Parse(t)
	if err != nil {
		return "", err
	}
	buf := &bytes.Buffer{}
	err = tpl.Execute(buf, i)
	return buf.String(), err
}
