package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

func TestIsLinkedDir(t *testing.T) {
	zone = time.UTC
	d, err := setupDirs()
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(d)
	path := filepath.Join(d, "dir", "link")
	ok, err := isLinkedDirWithErr(path)
	if err != nil {
		t.Fatal(err)
	} else if !ok {
		t.Errorf("expected %s to be a linked dir", path)
	}
}

func TestRenderDir(t *testing.T) {
	os.Setenv("TZ", "UTC")
	zone = time.UTC
	time.Local = zone
	d, err := setupDirs()
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(d)
	buf := &bytes.Buffer{}

	ex := "<pre><a href=\"../\">../</a>\r\n<a href=\"hello.txt\">hello.txt</a>                                          02-Jan-2006 02:00                   5\r\n<a href=\"link/\">link/</a>                                              02-Jan-2006 04:00                   -\r\n<a href=\"sub/\">sub/</a>                                               02-Jan-2006 01:00                   -\r\n</pre><hr></body></html>"
	if err := renderDir(filepath.Join(d, "dir"), "", buf); err != nil {
		t.Fatal(err)
	}
	if !strings.HasSuffix(buf.String(), ex) {
		t.Errorf("expected (in zone %s) \n%q\nto have suffix\n%q\n", time.Now().Format("-07:00"), buf.String(), ex)
	}
}

func TestMakeLink(t *testing.T) {
	tests := []struct {
		Name     string
		Expected interface{}
	}{
		{"some name", "<a href=\"some%20name\">some name</a>                                          "},
	}

	for _, tst := range tests {
		v := makeLink(tst.Name)
		if tst.Expected != v {
			t.Errorf("expected %s to be %#v, was %#v", tst.Name, tst.Expected, v)
		}
	}
}

func setupDirs() (string, error) {
	d, err := ioutil.TempDir("/tmp", "ts-fs-")
	if err != nil {
		return "", err
	}
	return d, func() error {
		if err := mkdir(filepath.Join(d, "dir", "sub"), date(2006, 1, 2, 1)); err != nil {
			return err
		}
		if err := writeFile(filepath.Join(d, "dir", "hello.txt"), "world", date(2006, 1, 2, 2)); err != nil {
			return err
		}
		if err := writeFile(filepath.Join(d, "dir", "sub", "hello2.txt"), "world2", date(2006, 1, 2, 3)); err != nil {
			return err
		}
		if err := symlink(filepath.Join(d, "dir", "sub"), filepath.Join(d, "dir", "link"), date(2006, 1, 2, 4)); err != nil {
			return err
		}
		return touch(filepath.Join(d, "dir", "sub"), date(2006, 1, 2, 1))
	}()
}

func touch(path string, modTime time.Time) error {
	fmt.Printf("touching %s with timestamp %s\n", path, modTime.Format(time.RFC3339Nano))
	b, err := exec.Command("touch", "-h", "-t", modTime.In(zone).Format("200601021504"), path).CombinedOutput()
	if err != nil {
		return fmt.Errorf("%s: %s", b, err)
	}
	return nil
}

func date(year, month, day, hour int) time.Time {
	return time.Date(year, time.Month(month), day, hour, 0, 0, 0, zone)
}

func symlink(oldname, newname string, modTime time.Time) error {
	if err := os.Symlink(oldname, newname); err != nil {
		return err
	}
	return touch(newname, modTime)
}

func mkdir(path string, modTime time.Time) error {
	if err := os.MkdirAll(path, 0755); err != nil {
		return err
	}
	return touch(path, modTime)
}

func writeFile(path string, content string, modTime time.Time) error {
	if err := ioutil.WriteFile(path, []byte(content), 0644); err != nil {
		return err
	}
	return touch(path, modTime)
}
